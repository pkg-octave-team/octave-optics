octave-optics (0.1.4-4) unstable; urgency=medium

  * d/rules: Use execute_after_dh_auto_build instead of override_dh_auto_build
  * d/watch:
    + Bump file version to 4
    + Adjust for new URL at gnu-octave.github.io
  * d/copyright:
    + Accentuate my family name
    + Update Copyright years for debian/* files
  * Set upstream metadata fields: Archive.
  * d/control: Bump Standards-Version to 4.6.1 (no changes needed)
  * Build-depend on dh-sequence-octave
    + d/control: Ditto
    + d/rules: Drop the --with=octave option from dh call
  * d/s/lintian-override: Override Lintian warning
    debian-watch-lacks-sourceforge-redirector

 -- Rafael Laboissière <rafael@debian.org>  Mon, 05 Dec 2022 10:23:37 -0300

octave-optics (0.1.4-3) unstable; urgency=medium

  * d/control: Bump debhelper compatibility level to 13
  * d/u/metadata: New file

 -- Rafael Laboissière <rafael@debian.org>  Wed, 29 Jul 2020 03:54:25 -0300

octave-optics (0.1.4-2) unstable; urgency=medium

  * Team Upload.

  [ Rafael Laboissière ]
  * d/control: Bump Standards-Version to 4.5.0 (no changes needed)

  [ Nilesh Patra ]
  * Update Build-Depends (Closes: #952241)
  * Switch URL to https
  * Fix control with cme

 -- Nilesh Patra <npatra974@gmail.com>  Fri, 28 Feb 2020 07:33:42 +0530

octave-optics (0.1.4-1) unstable; urgency=medium

  * New upstream version 0.1.4
  * d/copyright: Reflect upstream changes
  * d/control: Bump Standards-Version to 4.4.0 (no changes needed)
  * Build and install doc for zernikes_and_derivatives_cartesian_OSA function

 -- Rafael Laboissiere <rafael@debian.org>  Sun, 22 Sep 2019 20:59:59 -0300

octave-optics (0.1.3-2) unstable; urgency=medium

  * d/control:
    + Add Rules-Requires-Root: no
    + Bump Standards-Version to 4.3.0
    + Bump to debhelper compat level 12
  * Build-depend on debhelper-compat instead of using d/compat

 -- Rafael Laboissiere <rafael@debian.org>  Wed, 02 Jan 2019 22:57:08 -0200

octave-optics (0.1.3-1) unstable; urgency=low

  * Initial release (closes: #900683)

 -- Rafael Laboissiere <rafael@debian.org>  Sat, 30 Jun 2018 10:22:27 -0300
